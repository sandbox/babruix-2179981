<?php

/**
 * @file
 * Functions to query and render users statistics.
 */

$css_path = drupal_get_path('module', 'user_trend') . '/user_trend_style.css';
drupal_add_css($css_path);
define('UT_YEAR_START', 2014);
define('UT_YEAR_END', 2020);

/**
 * Page callback to show Yearly trend of user login.
 */
function user_trend_yearly_trend_report_page() {
  $form = drupal_get_form('user_trend_yearly_trend_form');
  $form['#attributes'] = array('class' => array('user_trend_wrapper'));
  $page = drupal_render($form);
  $page .= user_trend_yearly_trend_report();
  return $page;
}

/**
 * @param $form
 * @return mixed
 */
function user_trend_yearly_trend_form($form) {
  // Find out the current year
  $var_from_year = (arg(4) == '') ? date('Y') : arg(4);
  $var_to_year = (arg(5) == '') ? date('Y') : arg(5);

  $form['sel_from_year'] = array(
    '#title' => t('Select From Year'),
    '#type' => 'select',
    '#default_value' => (arg(4) != '') ? arg(4) : $var_from_year,
    '#options' => user_trend__get_years_options(),
  );
  $form['sel_to_year'] = array(
    '#title' => t('Select To Year'),
    '#type' => 'select',
    '#default_value' => (arg(5) != '') ? arg(5) : $var_to_year,
    '#options' => user_trend__get_years_options(),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}

/**
 * Implementation of hook_validate().
 */
function user_trend_yearly_trend_form_validate($form, &$form_state) {
  $from_year = $form_state['values']['sel_from_year'];
  $to_year = $form_state['values']['sel_to_year'];
  if ($from_year > $to_year) {
    form_set_error('sel_from_year',
      t("'From Year' can't be greater than 'To Year'"));
  }
}

/**
 * Implementation of hook_submit().
 */
function user_trend_yearly_trend_form_submit($form, &$form_state) {
  $from_year = $form_state['values']['sel_from_year'];
  $to_year = $form_state['values']['sel_to_year'];
  $url = "admin/reports/user_trend/yearly/" . $from_year . '/' . $to_year;
  drupal_goto($url);
}

function user_trend_yearly_trend_report() {
  $var_from_year = arg(4) == '' ? date('Y') : arg(4);
  $var_to_year = arg(5) == '' ? date('Y') : arg(5);

  $header[] = array('data' => t('Year'));
  $header[] = array('data' => t('Total User Logins'));

  $query = db_select('user_trend', 'ut');
  $query->addExpression("FROM_UNIXTIME(login_time, '%Y')", 'login_date');
  $query->addExpression("COUNT(*)", 'value_count');
  $query->where("FROM_UNIXTIME(login_time,'%Y') >= :from_year
    AND FROM_UNIXTIME(login_time,'%Y') <= :to_year ",
    array(':from_year' => $var_from_year, 'to_year' => $var_to_year)
  )->groupBy("FROM_UNIXTIME(login_time,'%Y')")->orderBy('login_time');
  $result = $query->extend('PagerDefault')->limit(31)->execute();

  $rows = user_trend__get_rows_from_result($result);
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output . theme('pager');
}

/*****************************************************************************/

/**
 * Page callback to show Quarterly trend of user login
 */
function user_trend_quarterly_trend_report_page() {
  $form = drupal_get_form('user_trend_quarterly_trend_form');
  $form['#attributes'] = array('class' => array('user_trend_wrapper'));
  $page = drupal_render($form);
  $page .= user_trend_quarterly_trend_report();
  return $page;
}

function user_trend_quarterly_trend_form($form) {
  // Find out the current year
  $var_year = (arg(4) == '') ? date('Y') : arg(4);
  $var_quarter = (arg(5) == '') ? '1' : arg(5);

  $form['sel_year'] = array(
    '#title' => t('Select Year'),
    '#type' => 'select',
    '#default_value' => (arg(4) != '') ? arg(4) : $var_year,
    '#options' => user_trend__get_years_options(),
  );
  $form['sel_quarter'] = array(
    '#title' => t('Select Quarter'),
    '#type' => 'select',
    '#default_value' => (arg(5) != '') ? arg(5) : $var_quarter,
    '#options' => array(
      '1' => t('Quarter 1'),
      '2' => t('Quarter 2'),
      '3' => t('Quarter 3'),
      '4' => t('Quarter 4'),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}

/**
 * Implementation of hook_submit().
 */
function user_trend_quarterly_trend_form_submit($form, &$form_state) {
  $year = $form_state['values']['sel_year'];
  $quarter = $form_state['values']['sel_quarter'];
  $url = 'admin/reports/user_trend/quarterly/' . $year . '/' . $quarter;
  drupal_goto($url);
}

function user_trend_quarterly_trend_report() {
  $var_year = arg(4) == '' ? date('Y') : arg(4);
  $var_quarter = arg(5) == '' ? 1 : arg(5);

  $quarter_array = user_trend__get_quarter_period($var_quarter, $var_year);
  $from_timestamp = $quarter_array['start'];
  $to_timestamp = $quarter_array['end'];

  $header[] = array('data' => t('Month'));
  $header[] = array('data' => t('Total User Logins'));

  $query = db_select('user_trend', 'ut');
  $query->addExpression("FROM_UNIXTIME(login_time, '%M, %Y')", 'login_date');
  $query->addExpression("COUNT(*)", 'value_count');
  $query->where("login_time >= :from AND login_time <= :to ",
    array(':from' => $from_timestamp, 'to' => $to_timestamp)
  )->groupBy("FROM_UNIXTIME(login_time,'%m %Y')")->orderBy('login_time');
  $result = $query->extend('PagerDefault')->limit(31)->execute();
  $rows = user_trend__get_rows_from_result($result);
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output . theme('pager');
}

/*****************************************************************************/

/**
 * Page callback to show Monthly trend of user login
 */
function user_trend_monthly_trend_report_page() {
  $form = drupal_get_form('user_trend_monthly_trend_form');
  $form['#attributes'] = array('class' => array('user_trend_wrapper'));
  $page = drupal_render($form);
  $page .= user_trend_monthly_trend_report();
  return $page;
}

function user_trend_monthly_trend_form($form) {
  // Find out the current year & month
  $var_from_mon = (arg(4) == '') ? date('m') : arg(4);
  $var_from_year = (arg(5) == '') ? date('Y') : arg(5);
  $var_to_mon = (arg(6) == '') ? date('m') : arg(6);
  $var_to_year = (arg(7) == '') ? date('Y') : arg(7);

  $form['sel_from_month'] = array(
    '#type' => 'select',
    '#title' => t('Select From Month & Year'),
    '#default_value' => (arg(4) != '') ? arg(4) : $var_from_mon,
    '#options' => user_trend__get_months_options(),
    /*'#description' => t('Select the month for displaying report .'),*/
  );
  $form['sel_from_year'] = array(
    '#type' => 'select',
    '#default_value' => (arg(5) != '') ? arg(5) : $var_from_year,
    '#options' => user_trend__get_years_options(),
    /*'#description' => t('Select the Year for displaying report .'),*/
  );
  $form['sel_to_month'] = array(
    '#type' => 'select',
    '#title' => t('Select To Month & Year'),
    '#default_value' => (arg(6) != '') ? arg(6) : $var_to_mon,
    '#options' => user_trend__get_months_options(),
    /*'#description' => t('Select the month for displaying report .'),*/
  );
  $form['sel_to_year'] = array(
    '#type' => 'select',
    '#default_value' => (arg(7) != '') ? arg(7) : $var_to_year,
    '#options' => user_trend__get_years_options(),
    /*'#description' => t('Select the Year for displaying report .'),*/
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}

/**
 * Implementation of hook_validate().
 */
function user_trend_monthly_trend_form_validate($form, &$form_state) {
  $from_mon = $form_state['values']['sel_from_month'];
  $from_year = $form_state['values']['sel_from_year'];
  $to_mon = $form_state['values']['sel_to_month'];
  $to_year = $form_state['values']['sel_to_year'];

  if ($from_year > $to_year) {
    form_set_error('sel_from_year',
      t("'From Year' can't be greater than 'To Year'"));
  }
  elseif (($from_year == $to_year) && ($from_mon > $to_mon)) {
    form_set_error('sel_from_month',
      t("'From Month' can't be greater than 'To Month'"));
  }
}

/**
 * Implementation of hook_submit().
 */
function user_trend_monthly_trend_form_submit($form, &$form_state) {
  $from_mon = $form_state['values']['sel_from_month'];
  $from_year = $form_state['values']['sel_from_year'];
  $to_mon = $form_state['values']['sel_to_month'];
  $to_year = $form_state['values']['sel_to_year'];
  $url = 'admin/reports/user_trend/monthly/' . $from_mon . '/'
    . $from_year . '/' . $to_mon . '/' . $to_year;
  drupal_goto($url);
}

function user_trend_monthly_trend_report() {
  $var_from_mon = arg(4) == '' ? date('m') : arg(4);
  $var_from_year = arg(5) == '' ? date('Y') : arg(5);
  $var_to_mon = arg(6) == '' ? date('m') : arg(6);
  $var_to_year = arg(7) == '' ? date('Y') : arg(7);
  $from_date_time_str = $var_from_year . '-'
    . $var_from_mon . '-'
    . user_trend__first_of_month($var_from_mon, $var_from_year)
    . '  00:00:00';
  $from_timestamp = strtotime($from_date_time_str);
  $to_date_time_str = $var_to_year
    . '-' . $var_to_mon
    . '-' . user_trend__last_of_month($var_to_mon, $var_to_year)
    . '  23:59:00';
  $to_timestamp = strtotime($to_date_time_str);

  $header[] = array('data' => t('Month'));
  $header[] = array('data' => t('Total User Logins'));

  $query = db_select('user_trend', 'ut');
  $query->addExpression("FROM_UNIXTIME(login_time, '%M, %Y')", 'login_date');
  $query->addExpression("COUNT(*)", 'value_count');
  $query->where("login_time >= :from AND login_time <= :to ",
    array(':from' => $from_timestamp, 'to' => $to_timestamp)
  )->groupBy("FROM_UNIXTIME(login_time,'%m %Y')")->orderBy('login_time');
  $result = $query->extend('PagerDefault')->limit(31)->execute();

  $rows = user_trend__get_rows_from_result($result);
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output . theme('pager');
}

/*****************************************************************************/

/**
 * Page callback to show Monthly trend of user login
 */
function user_trend_weekly_trend_report_page() {
  $form = drupal_get_form('user_trend_weekly_trend_form');
  $form['#attributes'] = array('class' => array('user_trend_wrapper'));
  $page = drupal_render($form);
  $page .= user_trend_weekly_trend_report();

  return $page;
}

function user_trend_weekly_trend_form($form) {
  // Find out the current year & month
  $var_from_mon = arg(4) == '' ? date('m') : arg(4);
  $var_from_year = arg(5) == '' ? date('Y') : arg(5);
  $var_to_mon = arg(6) == '' ? date('m') : arg(6);
  $var_to_year = arg(7) == '' ? date('Y') : arg(7);
  $var_from_week = arg(8) == '' ? 1 : arg(8);
  $var_to_week = arg(9) == '' ? 1 : arg(9);

  $form['sel_from_week'] = array(
    '#type' => 'select',
    '#title' => t('Select From Week, Month and Year'),
    '#default_value' => $var_from_week,
    '#options' => user_trend__get_weeks_options(),
    /*'#description' => t('Select the week for displaying report .'),*/
  );
  $form['sel_from_month'] = array(
    '#type' => 'select',
    '#default_value' => $var_from_mon,
    '#options' => user_trend__get_months_options(),
    /*'#description' => t('Select the month for displaying report .'),*/
  );
  $form['sel_from_year'] = array(
    '#type' => 'select',
    '#default_value' => $var_from_year,
    '#options' => user_trend__get_years_options(),
    /*'#description' => t('Select the Year for displaying report .'),*/
  );
  $form['sel_to_week'] = array(
    '#type' => 'select',
    '#title' => t('Select To Week, Month and Year'),
    '#default_value' => $var_to_week,
    '#options' => user_trend__get_weeks_options(),
    /*'#description' => t('Select the month for displaying report .'),*/
  );
  $form['sel_to_month'] = array(
    '#type' => 'select',
    '#default_value' => $var_to_mon,
    '#options' => user_trend__get_months_options(),
    /*'#description' => t('Select the month for displaying report .'),*/
  );
  $form['sel_to_year'] = array(
    '#type' => 'select',
    '#default_value' => $var_to_year,
    '#options' => user_trend__get_years_options(),
    /*'#description' => t('Select the Year for displaying report .'),*/
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}

/**
 * Implementation of hook_validate().
 */
function user_trend_weekly_trend_form_validate($form, &$form_state) {
  $from_week = $form_state['values']['sel_from_week'];
  $from_mon = $form_state['values']['sel_from_month'];
  $from_year = $form_state['values']['sel_from_year'];
  $to_week = $form_state['values']['sel_to_week'];
  $to_mon = $form_state['values']['sel_to_month'];
  $to_year = $form_state['values']['sel_to_year'];

  if ($from_year > $to_year) {
    form_set_error('sel_from_year',
      t("'From Year' can't be greater than 'To Year'"));
  }
  if ($from_year == $to_year && $from_mon > $to_mon) {
    form_set_error('sel_from_month',
      t("'From Month' can't be greater than 'To Month'"));
  }
  if ($from_year == $to_year && $from_mon == $to_mon && $from_week > $to_week) {
    form_set_error('sel_from_week',
      t("'From Week' can't be greater than 'To Week'"));
  }
}

/**
 * Implementation of hook_submit().
 */
function user_trend_weekly_trend_form_submit($form, &$form_state) {
  $from_mon = $form_state['values']['sel_from_month'];
  $from_year = $form_state['values']['sel_from_year'];
  $to_mon = $form_state['values']['sel_to_month'];
  $to_year = $form_state['values']['sel_to_year'];
  $from_week = $form_state['values']['sel_from_week'];
  $to_week = $form_state['values']['sel_to_week'];
  $url = 'admin/reports/user_trend/weekly/' . $from_mon .
    '/' . $from_year . '/' . $to_mon . '/' . $to_year .
    '/' . $from_week . '/' . $to_week;
  drupal_goto($url);
}

function user_trend_weekly_trend_report() {
  $var_from_mon = arg(4) == '' ? date('m') : arg(4);
  $var_from_year = arg(5) == '' ? date('Y') : arg(5);
  $var_to_mon = arg(6) == '' ? date('m') : arg(6);
  $var_to_year = arg(7) == '' ? date('Y') : arg(7);
  $from_week = arg(8) == '' ? '01' : arg(8);
  $to_week = arg(9) == '' ? '01' : arg(9);

  $from_year_month = $var_from_year . '-' . $var_from_mon . '-';
  $from_date = $from_year_month
    . user_trend__first_of_week($from_week, $var_from_mon, $var_from_year);
  $from_timestamp = strtotime($from_date . '  00:00:00');
  $to_year_month = $var_to_year . '-' . $var_to_mon . '-';
  $to_date = $to_year_month
    . user_trend__last_of_week($to_week, $var_to_mon, $var_to_year);
  $to_timestamp = strtotime($to_date . '  23:59:00');

  $header[] = array('data' => t('Day'));
  $header[] = array('data' => t('Total User Logins'));

  $query = db_select('user_trend', 'ut');
  $query->addExpression("FROM_UNIXTIME(login_time, '%d, %M, %Y')", 'login_date');
  $query->addExpression("COUNT(*)", 'value_count');
  $query->where("login_time >= :from AND login_time <= :to ",
    array(':from' => $from_timestamp, ':to' => $to_timestamp)
  )->groupBy("FROM_UNIXTIME(login_time,'%d, %M, %Y')")->orderBy('login_time');
  $result = $query->extend('PagerDefault')->limit(31)->execute();

  $rows = user_trend__get_rows_from_result($result);
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output . theme('pager');
}

/*****************************************************************************/

/**
 * Page callback to show Monthly trend of user login
 */
function user_trend_daily_trend_report_page() {
  $form = drupal_get_form('user_trend_daily_trend_form');
  $form['#attributes'] = array('class' => array('user_trend_wrapper'));
  $page = drupal_render($form);
  $page .= user_trend_daily_trend_report();

  return $page;
}

function user_trend_daily_trend_form($form) {
  // Find out the current year & month
  $var_from_mon = arg(4) == '' ? date('m') : arg(4);
  $var_from_year = arg(5) == '' ? date('Y') : arg(5);
  $var_to_mon = arg(6) == '' ? date('m') : arg(6);
  $var_to_year = arg(7) == '' ? date('Y') : arg(7);
  $var_from_day = arg(8) == '' ? 1 : arg(8);
  $var_to_day = arg(9) == '' ? 1 : arg(9);

  $form['sel_from_day'] = array(
    '#type' => 'select',
    '#title' => t('Select From Day, Week, Month and Year'),
    '#default_value' => $var_from_day,
    '#options' => user_trend__get_days_options(),
    /*'#description' => t('Select the week for displaying report .'),*/
  );
  $form['sel_from_month'] = array(
    '#type' => 'select',
    '#default_value' => $var_from_mon,
    '#options' => user_trend__get_months_options(),
    /*'#description' => t('Select the month for displaying report .'),*/
  );
  $form['sel_from_year'] = array(
    '#type' => 'select',
    '#default_value' => $var_from_year,
    '#options' => user_trend__get_years_options(),
    /*'#description' => t('Select the Year for displaying report .'),*/
  );

  $form['sel_to_day'] = array(
    '#type' => 'select',
    '#title' => t('Select To Day, Week, Month and Year'),
    '#default_value' => $var_to_day,
    '#options' => user_trend__get_days_options(),
    /*'#description' => t('Select the month for displaying report .'),*/
  );
  $form['sel_to_month'] = array(
    '#type' => 'select',
    '#default_value' => $var_to_mon,
    '#options' => user_trend__get_months_options(),
    /*'#description' => t('Select the month for displaying report .'),*/
  );
  $form['sel_to_year'] = array(
    '#type' => 'select',
    '#default_value' => $var_to_year,
    '#options' => user_trend__get_years_options(),
    /*'#description' => t('Select the Year for displaying report .'),*/
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Go'),
  );

  return $form;
}

/**
 * Implementation of hook_validate().
 */
function user_trend_daily_trend_form_validate($form, &$form_state) {
  $from_day = $form_state['values']['sel_from_day'];
  $from_mon = $form_state['values']['sel_from_month'];
  $from_year = $form_state['values']['sel_from_year'];
  $to_day = $form_state['values']['sel_to_day'];
  $to_mon = $form_state['values']['sel_to_month'];
  $to_year = $form_state['values']['sel_to_year'];

  if ($from_year > $to_year) {
    form_set_error('sel_from_year',
      t("'From Year' can't be greater than 'To Year'"));
  }
  if ($from_year == $to_year && $from_mon > $to_mon) {
    form_set_error('sel_from_month',
      t("'From Month' can't be greater than 'To Month'"));
  }
  if ($from_year == $to_year && $from_mon == $to_mon && $from_day > $to_day) {
    form_set_error('sel_from_day',
      t("'From Day' can't be greater than 'To Day'"));
  }
}

/**
 * Implementation of hook_submit().
 */
function user_trend_daily_trend_form_submit($form, &$form_state) {
  $from_mon = $form_state['values']['sel_from_month'];
  $from_year = $form_state['values']['sel_from_year'];
  $to_mon = $form_state['values']['sel_to_month'];
  $to_year = $form_state['values']['sel_to_year'];
  $from_day = $form_state['values']['sel_from_day'];
  $to_day = $form_state['values']['sel_to_day'];
  $url = 'admin/reports/user_trend/daily/' . $from_mon . '/' . $from_year
    . '/' . $to_mon . '/' . $to_year . '/'
    . $from_day . '/' . $to_day;
  drupal_goto($url);
}

function user_trend_daily_trend_report() {
  $var_from_mon = arg(4) == '' ? date('m') : arg(4);
  $var_from_year = arg(5) == '' ? date('Y') : arg(5);
  $var_to_mon = arg(6) == '' ? date('m') : arg(6);
  $var_to_year = arg(7) == '' ? date('Y') : arg(7);
  $from_day = arg(8) == '' ? '01' : arg(8);
  $to_day = arg(9) == '' ? '01' : arg(9);

  $from_year_month_day = $var_from_year . '-' . $var_from_mon . '-' . $from_day;
  $from_timestamp = strtotime($from_year_month_day . '  00:00:00');
  $to_year_month_day = $var_to_year . '-' . $var_to_mon . '-' . $to_day;
  $to_timestamp = strtotime($to_year_month_day . '  23:59:00');

  $header[] = array('data' => t('Day'));
  $header[] = array('data' => t('Total User Logins'));

  $query = db_select('user_trend', 'ut');
  $query->addExpression("FROM_UNIXTIME(login_time, '%d, %M, %Y')", 'login_date');
  $query->addExpression("COUNT(*)", 'value_count');
  $query->where("login_time >= :from AND login_time <= :to ",
    array(':from' => $from_timestamp, ':to' => $to_timestamp)
  )->groupBy("FROM_UNIXTIME(login_time,'%d, %M, %Y')")->orderBy('login_time');
  $result = $query->extend('PagerDefault')->limit(31)->execute();

  $rows = user_trend__get_rows_from_result($result);
  $output = theme('table', array('header' => $header, 'rows' => $rows));
  return $output . theme('pager');
}

/*****************************************************************************/

/**
 *
 * @param $result
 *
 * @return array
 */
function user_trend__get_rows_from_result($result) {
  $rows = array();
  $num_rows = FALSE;
  foreach ($result as $row) {
    $rows[] = array(
      $row->login_date,
      $row->value_count,
    );
    $num_rows = TRUE;
  }
  if (!$num_rows) {
    $rows[][] = array(
      'data' => t('No user logged in during this period.'),
      'class' => 'report_empty_cell',
      'colspan' => 2,
    );
  }
  return $rows;
}

/**
 * Build the array for Year dropdown option
 *
 * @return array
 */
function user_trend__get_years_options() {
  $options = array();
  for ($i = UT_YEAR_START; $i <= UT_YEAR_END; $i++) {
    $options[$i] = $i;
  }
  return $options;
}

/**
 * Build the array for Month dropdown option
 *
 * @return array
 */
function user_trend__get_months_options() {
  return array(
    '01' => t('Jan'),
    '02' => t('Feb'),
    '03' => t('Mar'),
    '04' => t('Apr'),
    '05' => t('May'),
    '06' => t('Jun'),
    '07' => t('Jul'),
    '08' => t('Aug'),
    '09' => t('Sep'),
    '10' => t('Oct'),
    '11' => t('Nov'),
    '12' => t('Dec'),
  );
}

/**
 * Build the array for Week dropdown option
 *
 * @return array
 */
function user_trend__get_weeks_options() {
  $week = t('Week');
  return array(
    '01' => $week . ' 1',
    '02' => $week . ' 2',
    '03' => $week . ' 3',
    '04' => $week . ' 4',
  );
}

/**
 * Build the array for Week dropdown option
 *
 * @return array
 */
function user_trend__get_days_options() {
  $day = t('Day');
  $days = array();
  for ($i = 1; $i <= 31; $i++) {
    $days[$i] = $day . ' ' . $i;
  }
  return $days;
}

/**
 * Get first day of given week
 *
 * @param string $week
 * @param string $mon
 * @param string $yr
 *
 * @return bool|string
 */
function user_trend__first_of_week($week = '', $mon = '', $yr = '') {
  $week = $week == '' ? '01' : $week;
  $month = $mon == '' ? date('m') : $mon;
  $year = $yr == '' ? date('Y') : $yr;
  $day = user_trend_first_day_of_week($week);

  return date("d", strtotime($month . '/' . $day . '/' . $year . ' 00:00:00'));
}

/**
 * Get last day of given week
 *
 * @param string $week
 * @param string $mon
 * @param string $yr
 *
 * @return bool|string
 */
function user_trend__last_of_week($week = '', $mon = '', $yr = '') {
  $week = $week == '' ? '01' : $week;
  $month = $mon == '' ? date('m') : $mon;
  $year = $yr == '' ? date('Y') : $yr;
  $day = user_trend_last_day_of_week($week);

  return date("d", strtotime($month . '/' . $day . '/' . $year . ' 00:00:00'));
}

/**
 * Get last day of a week
 * @param $week
 * @return string
 */
function user_trend_last_day_of_week($week) {
  switch ($week) {
    case '01':
      return '07';
      break;

    case '02':
      return '14';
      break;

    case '03':
      return '21';
      break;

    case '04':
      return '31';
      break;
  }
}

/**
 * Get first day of a week
 * @param $week
 * @return string
 */
function user_trend_first_day_of_week($week) {
  switch ($week) {
    case '01':
      return '01';
      break;

    case '02':
      return '08';
      break;

    case '03':
      return '14';
      break;

    case '04':
      return '21';
      break;
  }
}

/**
 * Common Functions
 *
 * @param string $mon
 * @param string $yr
 *
 * @return bool|string
 */
function user_trend__first_of_month($mon = '', $yr = '') {
  $month = ($mon == '') ? date('m') : $mon;
  $year = ($yr == '') ? date('Y') : $yr;
  return date("d", strtotime($month . '/01/' . $year . ' 00:00:00'));
}

function user_trend__last_of_month($mon = '', $yr = '') {
  $month = ($mon == '') ? date('m') : $mon;
  $year = ($yr == '') ? date('Y') : $yr;
  $month_year_time = strtotime($month . '/01/' . $year . ' 00:00:00');
  $timestamp = strtotime('-1 second', strtotime('+1 month', $month_year_time));
  return date("d", $timestamp);
}

function user_trend__get_quarter_period($quarter_value = '', $year_value = '') {
  $quarter_array = array();
  switch ($quarter_value) {
    case 1:
      $start_time_str = $year_value . '-01-'
        . user_trend__first_of_month(01, $year_value) . '  00:00:00';
      $quarter_array['start'] = strtotime($start_time_str);
      $end_time_str = $year_value . '-03-'
        . user_trend__last_of_month(01, $year_value) . '  23:59:00';
      $quarter_array['end'] = strtotime($end_time_str);
      break;

    case 2:
      $start_time_str = $year_value . '-04-'
        . user_trend__first_of_month(01, $year_value) . '  00:00:00';
      $quarter_array['start'] = strtotime($start_time_str);
      $end_time_str = $year_value . '-06-'
        . user_trend__last_of_month(01, $year_value) . '  23:59:00';
      $quarter_array['end'] = strtotime($end_time_str);
      break;

    case 3:
      $start_time_str = $year_value . '-07-'
        . user_trend__first_of_month(01, $year_value) . '  00:00:00';
      $quarter_array['start'] = strtotime($start_time_str);
      $end_time_str = $year_value . '-09-'
        . user_trend__last_of_month(01, $year_value) . '  23:59:00';
      $quarter_array['end'] = strtotime($end_time_str);
      break;

    case 4:
      $start_time_str = $year_value . '-10-'
        . user_trend__first_of_month(01, $year_value) . '  00:00:00';
      $quarter_array['start'] = strtotime($start_time_str);
      $end_time_str = $year_value . '-12-'
        . user_trend__last_of_month(01, $year_value) . '  23:59:00';
      $quarter_array['end'] = strtotime($end_time_str);
      break;
  }
  return $quarter_array;
}

