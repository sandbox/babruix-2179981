CONTENTS OF THIS FILE
---------------------

  - Introduction
  - Installation
  - Usage


INTRODUCTION
------------

Current Maintainer: Alexey Romanov.
Contact via: http://www.babruix.com.ua/contact

The primary goal of User stats is to provide dayly login user statistics.



INSTALLATION
------------

User stats is installed in the same way as most other Drupal modules.
Here are the instructions for anyone unfamiliar with the process:

1. Copy this user_trend/ directory to your sites/SITENAME/modules directory.

2. Back in your Web browser, navigate to Administer -> Site building -> Modules
   then enable the User Trend module.



USAGE
-------------
0. User login info will be saved once per day for each user who logs in.
1. Go to Admin / Reports / User Trend.
2. Visit different tabs, use different filters to specify start and end dates.
